using UnityEngine;
using UnityEngine.InputSystem;
using GGJ2024.Gameplay;
using System;
using GGJ2024.Gameplay.State;
using Gameplay;
using System.Collections.Generic;
using DG.Tweening;

#pragma warning disable 4014

namespace GGJ2024.Input
{
    public class PlayerInputController : MonoBehaviour
    {
        [SerializeField] private PlayerInput playerInput;
        [SerializeField] private Unit unit;
        [SerializeField] private GameObject hitParticlesPrefab;
        [SerializeField] private GameObject failedHitParticle;
        private InputMaster inputMaster;
        private InputActionMap actionMap;

        private InputAction moveAction;
        private InputAction lowKickAction;
        private InputAction middleKickAction;
        private InputAction highKickAction;
        private InputAction jumpAction;


        protected void Awake()
        {
            Initialize();
        }

        private void Initialize()
        {
            if(GameplayManager.Instance.PlayerManager.GetPlayerCount() >= 2)
            {
                Destroy(gameObject);
                return;
            }

            if(!playerInput)
            {
                Debug.LogError("PlayerInputController: cannot find player input!");
                return;
            }

            GameplayManager.Instance.InputManager.ChangePlayerPrefab();

            inputMaster = new();
            inputMaster.Player.Enable();

            actionMap = playerInput.actions.FindActionMap("Player");
            moveAction = actionMap.FindAction("Move");
            lowKickAction = actionMap.FindAction("LowKick");
            middleKickAction = actionMap.FindAction("MiddleKick");
            highKickAction = actionMap.FindAction("HighKick");
            jumpAction = actionMap.FindAction("Jump");


            lowKickAction.performed += OnLowKick;
            middleKickAction.performed += OnMiddleKick;
            highKickAction.performed += OnHighKick;
            jumpAction.performed += OnJump;

            GameplayManager.Instance.PlayerManager.AddPlayer(unit);
        }

        private void OnJump(InputAction.CallbackContext context)
        {
            if((unit.StateMachine.CurrentState is Idle || unit.StateMachine.CurrentState is Running) && unit.Movement.IsGrounded())
            {
                unit.Movement.Jump();
                unit.StateMachine.SetState<Jumping>();
            }
        }

        private void OnHighKick(InputAction.CallbackContext context)
        {
            if(unit.StateMachine.CurrentState is not Kicking)
            {
                unit.StateMachine.SetState<Kicking>();
            }
            else
            {
                return;
            }

            Vector3 direction;
            if(unit.Direction == Unit.DirectionType.Left)
            {
                direction = Vector3.left;
            }
            else
            {
                direction = Vector3.right;
            }
            
            TryKick(direction, KickType.High);
        }

        private void OnMiddleKick(InputAction.CallbackContext context)
        {
            if(unit.StateMachine.CurrentState is not Kicking)
            {
                unit.StateMachine.SetState<Kicking>();
            }
            else
            {
                return;
            }

            Vector3 direction;
            if(unit.Direction == Unit.DirectionType.Left)
            {
                direction = Vector3.left;
            }
            else
            {
                direction = Vector3.right;
            }
            
            TryKick(direction, KickType.Medium);
        }

        private void OnLowKick(InputAction.CallbackContext context)
        {
            if(unit.StateMachine.CurrentState is not Kicking)
            {
                unit.StateMachine.SetState<Kicking>();
            }
            else
            {
                return;
            }

            Vector3 direction;
            if(unit.Direction == Unit.DirectionType.Left)
            {
                direction = Vector3.left;
            }
            else
            {
                direction = Vector3.right;
            }

            TryKick(direction, KickType.Low);
        }

        private void TryKick(Vector3 direction, KickType kickType)
        {
            var hit = Physics2D.CircleCast(transform.position, 0.75f, direction, 1.5f);
            Debug.DrawRay(transform.position, direction, Color.red, 1f);
            
            if(hit)
            {
                if(hit.collider.TryGetComponent(out Interactable interactable))
                {
                    if(unit.Direction == Unit.DirectionType.Left)
                    {
                        DOVirtual.DelayedCall(0.2f, () => 
                        {
                            if(!(interactable.IsLanded && interactable.Weight > 1.9 && interactable.Weight < 2.1))
                            {
                                Instantiate(hitParticlesPrefab, interactable.transform.position, Quaternion.identity);

                                if(interactable.IsLanded)
                                {
                                    interactable.transform.SetParent(null);
                                    interactable.Slot.Detach();
                                    interactable.Detach();
                                }
                                interactable.Kick(kickType, KickDirection.Left, unit.Team, transform);
                            }
                            else
                            {
                                Instantiate(failedHitParticle, interactable.transform.position, Quaternion.identity);
                            }
                            
                        });
                    }
                    else
                    {
                        DOVirtual.DelayedCall(0.2f, () => 
                        {
                            if(!(interactable.IsLanded && interactable.Weight > 1.9 && interactable.Weight < 2.1))
                            {
                                Instantiate(hitParticlesPrefab, interactable.transform.position, Quaternion.identity);

                                if(interactable.IsLanded)
                                {
                                    interactable.transform.SetParent(null);
                                    interactable.Slot.Detach();
                                    interactable.Detach();
                                }

                                interactable.Kick(kickType, KickDirection.Right, unit.Team, transform);
                            }

                            else
                            {
                                Instantiate(failedHitParticle, interactable.transform.position, Quaternion.identity);
                            }
                        });
                    }
                }
            }
        }

        private void FixedUpdate() 
        {
            UpdateMovement();
        }

        private void UpdateMovement()
        {
            if(unit.StateMachine.CurrentState is not Kicking && GameplayManager.Instance.State == GameplayManager.GameState.Playing)
            {
                var movementVector = GetMovementVector();
                unit.Movement.Move(movementVector);

                var moveDirection = new Vector3(movementVector.x, 0, movementVector.y);
                if(moveDirection.x > 0)
                {
                    unit.LookRight();
                }
                else if(moveDirection.x < 0)
                {
                    unit.LookLeft();
                }
            }

            if(GameplayManager.Instance.State == GameplayManager.GameState.Win)
            {
                unit.Movement.MoveTowardsFinish();
            }
        }

        private Vector2 GetMovementVector()
        {
            var movement = moveAction.ReadValue<Vector2>();
            var leftWall = CameraSideCollider.List.Find(x => x.SideValue == CameraSideCollider.Side.Left);
            if(leftWall)
            {
                if(transform.position.x < leftWall.transform.position.x + 1)
                {
                    movement.x = Mathf.Clamp(movement.x, 0, 1);
                }
            }

            var rightWall = CameraSideCollider.List.Find(x => x.SideValue == CameraSideCollider.Side.Right);
            if(rightWall)
            {
                if(transform.position.x > rightWall.transform.position.x - 1)
                {
                    movement.x = Mathf.Clamp(movement.x, -1, 0);
                }
            }

            return movement;
        }

        protected void OnDisable()
        {
            inputMaster.Player.Disable();

            lowKickAction.performed -= OnLowKick;
            middleKickAction.performed -= OnMiddleKick;
            highKickAction.performed -= OnHighKick;
            jumpAction.performed -= OnJump;
        }
    }
}