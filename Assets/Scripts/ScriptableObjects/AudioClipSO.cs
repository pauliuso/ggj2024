using UnityEngine;

[CreateAssetMenu(fileName = "AudioClipSO", menuName = "ScriptableObjects/AudioClipSO", order = 1)]
public class AudioClipSO : ScriptableObject
{
    public AudioClip AudioClip;
    public float Volume = 1f;
    public float Pitch = 1f;
    public float Delay = 0f;
}
