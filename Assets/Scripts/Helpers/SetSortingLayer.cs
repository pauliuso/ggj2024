using UnityEngine;

[ExecuteInEditMode]
public class SetSortingLayer : MonoBehaviour
{
    [SerializeField] private int layer;

    void Start()
    {
        var SetMeshRenderer = GetComponent<MeshRenderer>();
       
        SetMeshRenderer.sortingOrder = layer;  
    }
}
