using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using GGJ2024.Gameplay;
using UnityEngine;

public class AnimationEvents : MonoBehaviour
{
    public void PlaySfx(AudioClipSO clipSO)
    {
        if(!clipSO.AudioClip)
            return;

        GameplayManager.Instance.SoundManager.PlaySound(clipSO);
    }

    public void Eat()
    {
        Monster.Instance.Animator.SetTrigger("Smile");
        DOVirtual.DelayedCall(6f, () => 
        {
            Fadeout.Instance.FadeOut();
        });
    }

    public void SpawnRummbleParticles()
    {
        Instantiate(GameplayManager.Instance.RumbleParticlePrefab, Monster.Instance.transform.position + Vector3.up * 8f, Quaternion.identity);
        GameplayManager.Instance.BridgeToDisableOnEat1.SetActive(false);
        GameplayManager.Instance.BridgeToDisableOnEat2.SetActive(false);
    }

    public void KillPlayers()
    {
        GameplayManager.Instance.PlayerManager.KillPlayers();
    }
}