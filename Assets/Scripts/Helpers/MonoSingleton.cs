﻿using UnityEngine;

namespace GGJ2024.Helpers
{
    public class MonoSingleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        protected static T instance;
        public static T Instance
        {
            get
            {
                if(!instance)
                    instance = FindObjectOfType<T>();

                return instance;
            }
        }

        protected virtual void Awake()
        {
            if(instance && instance != this)
            {
                Debug.LogError($"Multiple MonoSingletons detected. Deleting {transform.name}.");
                Destroy(gameObject);
                return;
            }
            
            instance = this as T;
        }
    }
}