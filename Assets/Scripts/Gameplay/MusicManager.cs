using UnityEngine;

namespace GGJ2024.Gameplay
{
    public class MusicManager : MonoBehaviour
    {
        [SerializeField] private AudioSource musicSource;
        [SerializeField] private AudioClip music1;

        private void OnValidate() 
        {
            if(!musicSource)
                TryGetComponent(out musicSource);
        }
        
        public void PlayMusic(AudioClip musicClip)
        {
            musicSource.clip = musicClip;
            musicSource.Play();
        }

        public void PauseMusic()
        {
            musicSource.Pause();
        }

        public void StopMusic()
        {
            musicSource.Stop();
        }
    }
}