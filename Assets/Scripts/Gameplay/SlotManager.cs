using System.Collections.Generic;
using GGJ2024.Gameplay;
using GGJ2024.Helpers;
using UnityEngine;

namespace Gameplay
{
    public class SlotManager : MonoSingleton<SlotManager>
    {
        [SerializeField] private List<Slot> topSlots;
        [SerializeField] private List<Slot> bottomSlots;

        public List<Slot> GetNearbySlotsTop(Slot slot)
        {
            var id = topSlots.IndexOf(slot);
            var nearbySlots = new List<Slot>();

            if (id == 0)
            {
                nearbySlots.Add(topSlots[id + 1]);
            }
            else if (id == topSlots.Count - 1)
            {
                nearbySlots.Add(topSlots[id - 1]);
            }
            else
            {
                nearbySlots.Add(topSlots[id - 1]);
                nearbySlots.Add(topSlots[id + 1]);
            }

            nearbySlots.RemoveAll(x => x == null);

            return nearbySlots;
        }

        public List<Slot> GetNearbySlotsBottom(Slot slot)
        {
            var id = bottomSlots.IndexOf(slot);
            var nearbySlots = new List<Slot>();

            if (id == 0)
            {
                nearbySlots.Add(bottomSlots[id + 1]);
            }
            else if (id == bottomSlots.Count - 1)
            {
                nearbySlots.Add(bottomSlots[id - 1]);
            }
            else
            {
                nearbySlots.Add(bottomSlots[id - 1]);
                nearbySlots.Add(bottomSlots[id + 1]);
            }

            nearbySlots.RemoveAll(x => x == null);

            return nearbySlots;
        }

        public void CheckWinCondition()
        {
            foreach(var slot in topSlots)
                if(!slot.HasInteractable || (slot.HasInteractable && slot.CurrentInteractable is Bomb))
                    return;

            foreach(var slot in bottomSlots)
                if(!slot.HasInteractable || (slot.HasInteractable && slot.CurrentInteractable is Bomb))
                    return;

            GameplayManager.Instance.OnWin();
        }
    }
}