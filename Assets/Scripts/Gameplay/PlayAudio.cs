using System.Collections;
using System.Collections.Generic;
using GGJ2024.Gameplay;
using UnityEngine;

public class PlayAudio : MonoBehaviour
{
    [SerializeField] private List<AudioClipSO> audioClips;

    private void Start()
    {
        var audioClip = audioClips[Random.Range(0, audioClips.Count)];
        if(!audioClip.AudioClip)
            return;

        GameplayManager.Instance.SoundManager.PlaySound(audioClip);
    }
}
