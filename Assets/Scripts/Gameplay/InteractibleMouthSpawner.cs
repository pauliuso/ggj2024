using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using GGJ2024.Gameplay;
using System.Linq;

namespace Gameplay
{
    public class InteractableMouthSpawner : MonoBehaviour
    {
        [SerializeField] private List<Interactable> interactables;
        [SerializeField] private List<Interactable> hazardous;
        private bool side = false;
        private Tween spawnTween;

        private List<GameObject> objectsToSpawn = new();
        
        private void Start() 
        {
            spawnTween = DOVirtual.DelayedCall(5f, () => 
            {
                if (GetGroundedInteractables().Count < 6 && GameplayManager.Instance.State == GameplayManager.GameState.Playing)
                {
                    SpitObjects();
                }
            }).SetLoops(-1);
        }

        private List<Interactable> GetGroundedInteractables()
        {
            var groundedInteractables = new List<Interactable>();
            var allInteractables = FindObjectsOfType<Interactable>();
            foreach (var interactable in allInteractables)
            {
                if (!interactable.IsLanded)
                    groundedInteractables.Add(interactable);
            }
            return groundedInteractables;
        }

        private void SpitObjects()
        {
   
            Monster.Instance.Animator.SetTrigger("Shout");

            int numberOfObjectsToSpawn = Random.Range(4, 6);

            objectsToSpawn = new List<GameObject>();

            for (int i = 0; i < numberOfObjectsToSpawn; i++)
            {
                var randomIndex = Random.Range(0, interactables.Count);
                objectsToSpawn.Add(interactables[randomIndex].gameObject);
            }

            objectsToSpawn.Add(hazardous[0].gameObject);    
            numberOfObjectsToSpawn += 1;

            DOVirtual.DelayedCall(0.4f, () => 
            {
                CameraScreenshake.Instance.Shake(1.5f);
                for (int i = 0; i < numberOfObjectsToSpawn; i++)
                {
                    var obj = objectsToSpawn[i];
                    DOVirtual.DelayedCall(0.15f * i, () => Spit());
                }
            });
        }

        private void Spit()
        {
            var objectToSpawn = objectsToSpawn.First();
            if(objectToSpawn == null)
            {
                return;
            }

            objectsToSpawn.Remove(objectToSpawn);

            var instance = Instantiate(objectToSpawn, transform.position, Quaternion.identity);
            instance.name = objectToSpawn.name;

            instance.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);

            if(instance.TryGetComponent<Interactable>(out var interactable))
            {
                side = !side;
                interactable.Spit(side);
                interactable.transform.localScale = Vector3.zero;
                interactable.transform.DOScale(Vector3.one, 0.5f);
            }
        }

        private void OnDestroy() 
        {
            spawnTween.Kill();
        }
    }
}
