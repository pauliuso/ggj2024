using System.Collections;
using System.Collections.Generic;
using GGJ2024.Helpers;
using UnityEngine;
using UnityEngine.InputSystem;

namespace GGJ2024.Gameplay
{
    public class PlayerManager : MonoSingleton<PlayerManager>
    {
        private List<Unit> playerList = new();
        [SerializeField] private Transform player1SpawnPoint;
        [SerializeField] private Transform player2SpawnPoint;

        public List<Unit> GetPlayers()
        {
            return playerList;
        }

        public bool TryGetPlayer(int index, out Unit unit)
        {
            unit = null;

            if(index >= playerList.Count)
                return false;

            unit = playerList[index];
            return unit;
        }

        public void AddPlayer(Unit unit)
        {
            playerList.Add(unit);

            if(!unit.TryGetComponent<Rigidbody>(out var rb))
            {
                Debug.LogError("PlayerManager: cannot find rigidbody2d on player!", rb.gameObject);
                return;
            }

            if(GetPlayerCount() == 1)
            {
                unit.transform.position = player1SpawnPoint.position;
                rb.position = player1SpawnPoint.position;
                unit.SetTeam(0);
            }
            else if(GetPlayerCount() == 2)
            {
                unit.transform.position = player2SpawnPoint.position;
                rb.position = player2SpawnPoint.position;
                unit.SetTeam(1);
            }
        }

        public int GetPlayerCount()
        {
            return playerList.Count;
        }

        public void KillPlayers()
        {
            foreach(var player in playerList)
            {
                Destroy(player.gameObject);
            }

            playerList.Clear();
        }

        public void DisableControls()
        {
            foreach(var player in playerList)
            {
                player.GetComponent<PlayerInput>().enabled = false;
            }
        }
    }
}
