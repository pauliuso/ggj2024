using System.Collections.Generic;
using GGJ2024.Gameplay.State;
using UnityEngine;

namespace GGJ2024.Gameplay
{
    public class Unit : MonoBehaviour
    {
        [SerializeField] private Movement movement;
        [SerializeField] private Animator animator;
        [SerializeField] private StateMachine stateMachine;
        [SerializeField] private Transform visual;
        [SerializeField] private DirectionType direction = DirectionType.Right;
        [SerializeField] private List<AudioClipSO> hitSounds;
        [SerializeField] private int team;

        public Movement Movement => movement;
        public Animator Animator => animator;
        public StateMachine StateMachine => stateMachine;
        public DirectionType Direction => direction;
        public List<AudioClipSO> HitSounds => hitSounds;
        public enum DirectionType { Left, Right }
        public int Team => team;

        public void GoIdle()
        {
            stateMachine.SetState<Idle>();
        }

        public void Land()
        {
            stateMachine.SetState<Landing>();
        }

        public void LookLeft()
        {
            direction = DirectionType.Left;
            transform.rotation = Quaternion.Euler(0, 0, 0);
        }

        public void LookRight()
        {
            direction = DirectionType.Right;
            transform.rotation = Quaternion.Euler(0, 180, 0);
        }

        public void SetTeam(int team)
        {
            this.team = team;
        }
    }
}