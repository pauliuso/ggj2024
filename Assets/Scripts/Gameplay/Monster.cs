using System.Collections.Generic;
using GGJ2024.Helpers;
using UnityEngine;


namespace GGJ2024.Gameplay
{
    public class Monster : MonoSingleton<Monster>
    {
        [SerializeField] private Animator animator;
        [SerializeField] private List<AudioClipSO> genericAttachSounds;
        [SerializeField] private GameObject insertParticlePrefab;

        public Animator Animator => animator;
        public List<AudioClipSO> GenericAttachSounds => genericAttachSounds;
        public GameObject InsertParticlePrefab => insertParticlePrefab;
    }
}