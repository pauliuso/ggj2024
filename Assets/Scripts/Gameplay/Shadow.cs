using UnityEngine;
using DG.Tweening;
using Gameplay;

public class Shadow : MonoBehaviour
{
    [SerializeField] private GameObject shadowVisual;
    [SerializeField] private LayerMask groundLayer;
    [SerializeField] private Interactable interactable;
    private SpriteRenderer spriteRenderer;
    private int playerLayer;

    private void OnValidate() 
    {
        TryGetComponent(out interactable);
    }

    private void Start() 
    {
        playerLayer = LayerMask.NameToLayer("Player");
        shadowVisual = Instantiate(shadowVisual, transform.position, Quaternion.identity);
        spriteRenderer = shadowVisual.GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        if(interactable && interactable.IsLanded)
        {
            shadowVisual.SetActive(false);
            return;
        }

        shadowVisual.SetActive(true);

        var hit = Physics2D.Raycast(transform.position, Vector2.down, 20f, groundLayer);

        if(hit)
        {
            if(hit.collider.gameObject.GetComponent<GroundBlock>())
            {
                if(this.gameObject.layer == playerLayer)
                {
                    shadowVisual.transform.position = new Vector3(hit.point.x, hit.point.y, transform.position.z) + Vector3.up * 0.3f;
                }
                else
                {
                    shadowVisual.transform.position = new Vector3(hit.point.x, hit.point.y, transform.position.z) + Vector3.up * 0.1f;
                }

                var dst = hit.distance;
                float alpha = 1f - Mathf.Clamp(dst / 10f, 0f, 1f);
                var currentColor = spriteRenderer.color;
                currentColor.a = alpha;
                spriteRenderer.color = currentColor;
                var scale = new Vector3(0.74f, 0.25f, 1f);
                spriteRenderer.transform.localScale = scale * Mathf.Clamp(1f - dst / 10f, 0.5f, 1f);
            }
        }
    }

    private void OnDestroy() 
    {
        Destroy(shadowVisual);
    }
}
