using UnityEngine;
using UnityEngine.InputSystem;

namespace GFJ2024.Gameplay
{
    public class InputManager : MonoBehaviour
    {
        [SerializeField] private GameObject player2Prefab;
        [SerializeField] private PlayerInputManager playerInputManager;

        public PlayerInputManager PlayerInputManager => playerInputManager;

        public void ChangePlayerPrefab()
        {
            playerInputManager.playerPrefab = player2Prefab;
        }
    }
}