using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

namespace GGJ2024.Gameplay
{
    public class SoundManager : MonoBehaviour
    {
        [SerializeField] private List<AudioSource> audioSources;
        private int currentAudioSourceIndex = 0;
        
        public void PlaySound(AudioClipSO clipSO)
        {
            if(!clipSO.AudioClip)
                return;

            var audioSource = audioSources[currentAudioSourceIndex];
            currentAudioSourceIndex++;
            if(currentAudioSourceIndex >= audioSources.Count)
                currentAudioSourceIndex = 0;

            DOVirtual.DelayedCall(clipSO.Delay, () => 
            {
                audioSource.pitch = clipSO.Pitch;
                audioSource.PlayOneShot(clipSO.AudioClip, clipSO.Volume);
            });
        }
    }
}

