using GFJ2024.Gameplay;
using DG.Tweening;
using GGJ2024.Helpers;
using GGJ2024.UI;
using UnityEngine;
using Gameplay;
using System.Linq;
using System.Collections.Generic;

namespace GGJ2024.Gameplay
{
    public class GameplayManager : MonoSingleton<GameplayManager>
    {
        [SerializeField] private UIManager uiManager;
        [SerializeField] private MusicManager musicManager;
        [SerializeField] private SoundManager soundManager;
        [SerializeField] private PlayerManager playerManager;
        [SerializeField] private InputManager inputManager;
        [SerializeField] private GameObject ricoshetteParticlePrefab;
        [SerializeField] private GameObject explosionParticlePrefab;
        [SerializeField] private GameObject rumbleParticlePrefab;
        [SerializeField] private GameObject bridgeToDisableOnEat1;
        [SerializeField] private GameObject bridgeToDisableOnEat2;
        [SerializeField] private GameObject player1FinishPoint;
        [SerializeField] private GameObject player2FinishPoint;
        [SerializeField] private GameObject monster;

        public enum GameState
        {
            Menu,
            Paused,
            Playing,
            Win,
            Reloading
        }

        public UIManager UIManager => uiManager;
        public MusicManager MusicManager => musicManager;
        public SoundManager SoundManager => soundManager;
        public PlayerManager PlayerManager => playerManager;
        public InputManager InputManager => inputManager;
        public GameObject RicoshetteParticlePrefab => ricoshetteParticlePrefab;
        public GameObject ExplosionParticlePrefab => explosionParticlePrefab;
        public GameObject RumbleParticlePrefab => rumbleParticlePrefab;
        public GameObject BridgeToDisableOnEat1 => bridgeToDisableOnEat1;
        public GameObject BridgeToDisableOnEat2 => bridgeToDisableOnEat2;
        public GameObject Player1FinishPoint => player1FinishPoint;
        public GameObject Player2FinishPoint => player2FinishPoint;
        public GameState State = GameState.Playing;
        public float Time;

        protected void Awake()
        {
            base.Awake();
            
            CameraSideCollider.List.Clear();

            Interactable.List.Clear();

            Fadeout.Instance.FadeIn();
        }

        private void Start() 
        {
            Application.targetFrameRate = 60;
            DOTween.Init();
        }

        private void Update() 
        {
            Time += UnityEngine.Time.deltaTime;

            if(UnityEngine.Input.GetKeyDown(KeyCode.Escape))
            {
                OnWin();
            }
            if(!monster.activeSelf)
            {
                if(PlayerManager.Instance.GetPlayerCount() > 1)
                {
                    monster.SetActive(true);
                } else if (PlayerManager.Instance.GetPlayerCount() == 1 && Time > 10f)
                {
                    monster.SetActive(true);
                }
            }
            
            if(Fadeout.Instance.IsFadedOut && State != GameState.Reloading)
            {
                State = GameState.Reloading;
                ReloadScene();

            }
        }

        public void OnWin()
        {
            Monster.Instance.Animator.SetTrigger("Back_To_Centr");
            State = GameState.Win;

            PlayerManager.DisableControls();

            List<Interactable> interactables = FindObjectsOfType<Interactable>().ToList();
            interactables = interactables.Where(x => !x.IsLanded).ToList();

            float delay = 0f;
            foreach (var interactable in interactables)
            {
                interactable.transform.DOScale(0f, 0.3f).SetEase(Ease.InOutBack).SetDelay(delay).OnComplete(() => 
                {
                    Destroy(interactable.gameObject);
                });
                delay += 0.1f;
            }

            InputManager.gameObject.SetActive(false);
        }

        public void ReloadScene()
        {
            Debug.Log("Reload scene");
            DOTween.KillAll();
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        }
    }
}