using UnityEngine;

namespace GGJ2024.Gameplay.State
{
    public class Running : State
    {
        public override void OnStart()
        {
            ownerStateMachine.Unit.Animator.SetBool("IsRunning", true);
            ownerStateMachine.Unit.Animator.SetTrigger("StartRunning");
        }

        public override void OnExit()
        {
            ownerStateMachine.Unit.Animator.SetBool("IsRunning", false);
        }

        public override void OnUpdate()
        {

        }
    }
}