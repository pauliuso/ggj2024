using UnityEngine;

namespace GGJ2024.Gameplay.State
{
    public class Win : State
    {
        public override void OnStart()
        {
            ownerStateMachine.Unit.Animator.SetTrigger("Win");
        }

        public override void OnExit()
        {

        }

        public override void OnUpdate()
        {
            
        }
    }
}