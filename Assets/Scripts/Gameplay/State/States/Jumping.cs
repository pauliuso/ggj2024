using UnityEngine;

namespace GGJ2024.Gameplay.State
{
    public class Jumping : State
    {
        public override void OnStart()
        {
            ownerStateMachine.Unit.Animator.SetTrigger("Jump");
        }

        public override void OnExit()
        {
            
        }

        public override void OnUpdate()
        {
            
        }
    }
}