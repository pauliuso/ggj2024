using UnityEngine;

namespace GGJ2024.Gameplay.State
{
    public class Walking : State
    {
        public override void OnStart()
        {
            ownerStateMachine.Unit.Animator.SetBool("IsWalking", true);
            ownerStateMachine.Unit.Animator.SetTrigger("StartWalking");
        }

        public override void OnExit()
        {
            ownerStateMachine.Unit.Animator.SetBool("IsWalking", false);
        }

        public override void OnUpdate()
        {
            
        }
    }
}