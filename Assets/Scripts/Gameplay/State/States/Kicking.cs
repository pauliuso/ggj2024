using UnityEngine;

namespace GGJ2024.Gameplay.State
{
    public class Kicking : State
    {
        public override void OnStart()
        {
            ownerStateMachine.Unit.Animator.SetTrigger("Kick");
            ownerStateMachine.Unit.Movement.Stop();

            if(ownerStateMachine.Unit.HitSounds.Count > 0)
            {
                GameplayManager.Instance.SoundManager.PlaySound(ownerStateMachine.Unit.HitSounds[Random.Range(0, ownerStateMachine.Unit.HitSounds.Count)]);
            }
        }

        public override void OnExit()
        {
        }

        public override void OnUpdate()
        {
            
        }
    }
}