using UnityEngine;

namespace GGJ2024.Gameplay.State
{
    public class Landing : State
    {
        public override void OnStart()
        {
            ownerStateMachine.Unit.Animator.SetTrigger("Land");
        }

        public override void OnExit()
        {
            
        }

        public override void OnUpdate()
        {
            
        }
    }
}