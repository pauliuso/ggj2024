using UnityEngine;

namespace GGJ2024.Gameplay.State
{
    public class Idle : State
    {
        public override void OnStart()
        {
            ownerStateMachine.Unit.Animator.SetBool("IsIdle", true);
        }

        public override void OnExit()
        {
            ownerStateMachine.Unit.Animator.SetBool("IsIdle", false);
        }

        public override void OnUpdate()
        {
            
        }
    }
}