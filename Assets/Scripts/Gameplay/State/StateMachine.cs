using UnityEngine;

namespace GGJ2024.Gameplay.State
{
    public class StateMachine : MonoBehaviour
    {
        [SerializeField] private State currentState;
        [SerializeField] private Unit unit;

        public State CurrentState => currentState;
        public Unit Unit => unit;

        private void OnValidate() 
        {
            if(!unit)
                unit = GetComponent<Unit>();
        }

        private void Start()
        {
            SetState<Idle>();
        }

        private void Update()
        {
            if(currentState)
            {
                currentState.OnUpdate();
            }
        }

        public void SetState<T>() where T : State
        {
            if(currentState)
                currentState.OnExit();

            var state = ScriptableObject.CreateInstance<T>();

            state.OnInit(this);
            state.OnStart();

            currentState = state;
        }  
    }
}