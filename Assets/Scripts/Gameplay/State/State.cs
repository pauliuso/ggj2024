using UnityEngine;

namespace GGJ2024.Gameplay.State
{
    public abstract class State : ScriptableObject
    {
        protected StateMachine ownerStateMachine;

        public virtual void OnInit(StateMachine stateMachine)
        {
            this.ownerStateMachine = stateMachine;
        }

        public abstract void OnStart();
        public abstract void OnExit();
        public abstract void OnUpdate();
    }
}