using UnityEngine;

namespace Gameplay
{
    public class Lower : Slot
    {
        protected override void OnTriggerEnter2D(Collider2D other)
        {
            if(other.TryGetComponent<Rigidbody2D>(out var rigidbody2D))
                if(rigidbody2D.velocity.y > 0)
                    return;

            if(!other.CompareTag(Tags.Interactable.ToString()))
                return;

            if(!other.TryGetComponent<Interactable>(out var interactable))
                return;

            Attach(interactable);
        }
    }
}
