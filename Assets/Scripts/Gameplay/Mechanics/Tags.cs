namespace Gameplay
{
    public enum Tags
    {
        Interactable,
        TopBoundary,
        SideBoundary,
        GroundBoundary
    }
}
