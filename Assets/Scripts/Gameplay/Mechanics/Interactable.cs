using System.Collections.Generic;
using GGJ2024.Gameplay;
using UnityEngine;

namespace Gameplay
{
    public enum KickDirection
    {
        Left,
        Right
    }

    public enum KickType
    {
        Low,
        Medium,
        High
    }

    public class Interactable : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer spriteRenderer;
        [SerializeField] private Sprite neutralSprite;
        [SerializeField] private List<Sprite> flyingSprite;
        [SerializeField] private List<Sprite> landedSprite;
        [SerializeField] private new Rigidbody2D rigidbody2D;
        [SerializeField] private new Collider2D collider2D;

        [SerializeField] private List<AudioClipSO> attachSounds;
        [SerializeField] private List<AudioClipSO> hitSounds;
        [SerializeField] private List<AudioClipSO> hitWallSounds;
        [SerializeField] private List<GameObject> trailParticle;
        [SerializeField] private float weight = 1;

        public int Team => team;

        [SerializeField] private int team = -1;
        [SerializeField] private bool gravityEnabled = false;
        [SerializeField] private float gravity = 5;
        [SerializeField] private float gravityIncrease = 1;
        [SerializeField] protected int groundHits = 0;
        [SerializeField] private bool isLanded = false;
        [SerializeField] private bool isFlying = false;
        [SerializeField] protected Slot slot;

        public bool IsLanded => isLanded;
        public Rigidbody2D Rigidbody2D => rigidbody2D;
        public float Weight => weight;
        public int GroundHits => groundHits;

        public static List<Interactable> List = new();

        private void Awake()
        {
            List.Add(this);
        }

        private void OnDestroy()
        {
            List.Remove(this);
        }

        public Slot Slot => slot;

        private void Start() 
        {
            foreach(var particle in trailParticle)
            {
                particle.SetActive(false);
            }
        }

        public virtual void Attached(Slot slot)
        {
       
        }

        public virtual void Detach()
        {
            slot = null;
        }

        public void SetSlot(Slot slot)
        {
            this.slot = slot;
        }

        public void KickBlow(Vector2 force)
        {
            if(isLanded)
                return;
                
            EnablePhysics();
            SpitForce(force);
            ResetTeam();
        }

        public void SpitForce(Vector2 force)
        {
            EnablePhysics();
            
            isFlying = false;
            groundHits = 0;

            var angularVelocity = Random.Range(360f, 720f);
            if(Random.Range(0, 2) == 0)
                angularVelocity *= -1;

            rigidbody2D.angularVelocity = angularVelocity;
            rigidbody2D.AddForce(force);

            gravityEnabled = true;

            gravity = GameplaySettings.Instance.Gravity;
            gravityIncrease = GameplaySettings.Instance.SpitGravityIncrease;
        }

        public void Spit(bool side)
        {
            EnablePhysics();
            
            isFlying = false;
            groundHits = 0;
            var force = new Vector2(Random.Range(0.025f, 0.75f), Random.Range(0.35f, 0.75f));
            if(side)
                force.x *= -1;
                
            force.Normalize();
            force *= 450f;

            var angularVelocity = Random.Range(360f, 720f);
            if(Random.Range(0, 2) == 0)
                angularVelocity *= -1;

            rigidbody2D.angularVelocity = angularVelocity;
            rigidbody2D.AddForce(force);

            gravityEnabled = true;

            gravity = GameplaySettings.Instance.Gravity;
            gravityIncrease = GameplaySettings.Instance.SpitGravityIncrease;
        }

        public bool IsGrounded()
        {
            return groundHits > 2;
        }

        public virtual void Kick(KickType forceType, KickDirection kickDirection, int team, Transform source)
        {
            EnablePhysics();

            if(hitSounds.Count > 0)
            {
                GameplayManager.Instance.SoundManager.PlaySound(hitSounds[Random.Range(0, hitSounds.Count)]);
            }

            this.gameObject.layer = LayerMask.NameToLayer("Interactible");

            groundHits = 0;
            this.team = team;

            if(trailParticle.Count > 0)
            {
                trailParticle[team].SetActive(true);
            }

            rigidbody2D.velocity = Vector2.zero;

            var force = GameplaySettings.Instance.KickForce;
            switch(forceType)
            {
                case KickType.High:
                    var forceY0 = force;
                    var forceX0 = forceY0 * 0.25f;

                    if(kickDirection == KickDirection.Left)
                        forceX0 *= -1;

                    rigidbody2D.AddForce(new Vector2(forceX0, forceY0)
                    {
                        x = forceX0,
                        y = forceY0
                    });

                break;

                case KickType.Medium:
                    var forceY = force;
                    var forceX = forceY * 0.5f;

                    if(kickDirection == KickDirection.Left)
                        forceX *= -1;

                    rigidbody2D.AddForce(new Vector2(forceX, forceY)
                    {
                        x = forceX,
                        y = forceY
                    });

                break;

                case KickType.Low:
                    var forceY2 = force;
                    var forceX2 = forceY2 * 1f;

                    if(kickDirection == KickDirection.Left)
                        forceX2 *= -1;

                    rigidbody2D.AddForce(new Vector2(forceX2, forceY2)
                    {
                        x = forceX2,
                        y = forceY2
                    });

                break;
            }

            gravity = GameplaySettings.Instance.Gravity;

            SetFlyingSprite(team);

            CameraScreenshake.Instance.Shake(0.3f);

            // kick nearby

            if(source)
            {
                var minimumDistance = 3f;

                foreach(var interactable in List)
                {
                    if(interactable == this)
                        continue;

                    var distance = Vector2.Distance(transform.position, interactable.transform.position);
                    if(distance < minimumDistance)
                    {
                        Vector2 dir = interactable.transform.position - source.position;
                        dir.y += 3f;
                        // Normalize

                        dir.Normalize();

                        var direction = dir.normalized;
                        var force2 = Random.Range(400, 600);

                        var finalForce = direction * force2;

                        // depend on distance

                        var distanceRatio = 1 - (distance / minimumDistance);

                        finalForce.x *= Mathf.Max(0.5f, distanceRatio);
                        finalForce.y *= Mathf.Max(0.75f, distanceRatio);

                        interactable.KickBlow(finalForce);
                    }
                }
            }
    
        }

        public void DisablePhysics()
        {
            rigidbody2D.bodyType = RigidbodyType2D.Static;
            collider2D.isTrigger = true;
        }

        public void EnablePhysics()
        {
            rigidbody2D.bodyType = RigidbodyType2D.Dynamic;
            collider2D.isTrigger = false;
        }

        public void ResetTeam()
        {
            team = -1;

            spriteRenderer.sprite = neutralSprite;
            spriteRenderer.sortingOrder = 550 + List.IndexOf(this);
            isLanded = false;
            isFlying = false;
        }

        private void GenerateRicochettedParticle(Vector3 position)
        {
            if(position.y < InteractibleGroundLine.Instance.transform.position.y)
                return;

            var bigHit = rigidbody2D.velocity.magnitude > 55f;

            var particle = Instantiate(GameplayManager.Instance.RicoshetteParticlePrefab, position, Quaternion.identity);
            if(!bigHit)
            {
                particle.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
                CameraScreenshake.Instance.Shake(0.2f);
            }
            else
            {
                CameraScreenshake.Instance.Shake(0.4f);
            }
        }

        private void OnCollisionEnter2D(Collision2D other)
        {
            isFlying = false;
            if(other.gameObject.CompareTag(Tags.Interactable.ToString()))
            {
                gravityEnabled = true;
                gravityIncrease = GameplaySettings.Instance.SpitGravityIncrease;
                
                var otherInteractible = other.gameObject.GetComponent<Interactable>();
                otherInteractible.gravityEnabled = true;
                otherInteractible.gravityIncrease = GameplaySettings.Instance.SpitGravityIncrease;

                CameraScreenshake.Instance.Shake(0.4f);
                return;
            }
            
            if(other.gameObject.CompareTag(Tags.TopBoundary.ToString()))
            {
                var contact = other.contacts[0];
                var position = transform.position;
                position.x = contact.point.x;
                position.y = contact.point.y;
                transform.position = position;
                
                GenerateRicochettedParticle(position);

                if(hitWallSounds.Count > 0)
                {
                    GameplayManager.Instance.SoundManager.PlaySound(hitWallSounds[Random.Range(0, hitSounds.Count)]);
                }
                // if(Mathf.Abs(rigidbody2D.velocity.x) < Mathf.Abs(rigidbody2D.velocity.y))
                // {
                    
                rigidbody2D.velocity *= 0.25f;
                gravityEnabled = true;
      
                gravityIncrease = GameplaySettings.Instance.SpitGravityIncrease;


                foreach(var particle in trailParticle)
                {
                    particle.SetActive(false);
                }

                return;
            }

            if(other.gameObject.CompareTag(Tags.SideBoundary.ToString()))
            {
                var contact = other.contacts[0];
                var position = transform.position;
                position.x = contact.point.x;
                position.y = contact.point.y;
                transform.position = position;

                gravityIncrease = 1;
                gravityEnabled = true;

                GenerateRicochettedParticle(position);

                if(hitWallSounds.Count > 0)
                {
                    GameplayManager.Instance.SoundManager.PlaySound(hitWallSounds[Random.Range(0, hitSounds.Count)]);
                }
            }

            if(other.gameObject.CompareTag(Tags.GroundBoundary.ToString()))
            {
                if(groundHits == 0)
                {
                    foreach(var particle in trailParticle)
                    {
                        particle.SetActive(false);
                    }

                    ResetTeam();
                }

                if(groundHits < 2)
                {
                    gravity = 15;
                    rigidbody2D.velocity *= 0.25f;
                    gravityEnabled = true;

                    if(hitWallSounds.Count > 0)
                    {
                        GameplayManager.Instance.SoundManager.PlaySound(hitWallSounds[Random.Range(0, hitSounds.Count)]);
                    }
                }
                else
                {
                    gravityIncrease = 1;
                    gravityEnabled = false;
                    rigidbody2D.velocity = Vector2.zero;
                    rigidbody2D.angularVelocity = 0;
                }
                
                if(groundHits == 2)
                {
                    DisablePhysics();   
                }
             
                groundHits++;;
                
                return;
            }
        }

        public void SetLanded(int team)
        {
            spriteRenderer.sortingOrder = 500 + List.IndexOf(this);
            spriteRenderer.sprite = landedSprite[team];
            isLanded = true;
            isFlying = false;

            transform.localRotation = Quaternion.identity;

            if(attachSounds.Count > 0)
            {
                GameplayManager.Instance.SoundManager.PlaySound(attachSounds[Random.Range(0, attachSounds.Count)]);
            }

            foreach(var particle in trailParticle)
            {
                particle.SetActive(false);
            }
        }

        public void SetFlyingSprite(int team)
        {
            EnablePhysics();
            
            spriteRenderer.sortingOrder = 600 + List.IndexOf(this);
            spriteRenderer.sprite = flyingSprite[team];
            isFlying = true;
        }

        protected virtual void FixedUpdate()
        {
            // set object rotation from it's XY velocity
            var velocity = rigidbody2D.velocity;
            var angle = Mathf.Atan2(velocity.y, velocity.x) * Mathf.Rad2Deg;

            if(isFlying)
            {
                transform.rotation = Quaternion.Euler(0, 0, angle + 90);
            }

            if(transform.position.y > InteractibleGroundLine.Instance.transform.position.y & team > -1)
            {
                gameObject.layer = LayerMask.NameToLayer("Interactible");
            }
            else
            {
                gameObject.layer = LayerMask.NameToLayer("InteractibleNeutral");
            }
             
            if(!gravityEnabled)
                return;

            var force = new Vector2(0, -gravity);

            if(groundHits < 2 && !isLanded)
            {
                gravity *= gravityIncrease;
            }

            force.y *= 1;

            rigidbody2D.AddForce(force);
        }

        internal object Where(System.Func<object, object> value)
        {
            throw new System.NotImplementedException();
        }

        protected virtual void Update()
        {
            if(transform.parent != null)
            {
                var ratio = transform.lossyScale.x / transform.lossyScale.y;
                spriteRenderer.transform.localScale = new Vector3(0.5f, ratio * 0.5f, 1);
            }
            else
            {
                spriteRenderer.transform.localScale = Vector3.one * 0.5f;
            }
        }
    }
}
