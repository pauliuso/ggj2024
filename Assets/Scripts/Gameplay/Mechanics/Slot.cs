using GGJ2024.Gameplay;
using UnityEngine;

namespace Gameplay
{
    public class Slot : MonoBehaviour
    {
        [SerializeField] private Interactable currentInteractable;

        public bool HasInteractable => currentInteractable != null;
        public Interactable CurrentInteractable => currentInteractable;

        public void Detach()
        {
            if(currentInteractable == null)
                return;
            
            if(currentInteractable.transform.parent)
            {
                currentInteractable.transform.parent = null;
            }
            
            currentInteractable.EnablePhysics();
            currentInteractable.Spit(Random.Range(0, 2) == 0);
            currentInteractable.ResetTeam();
            currentInteractable = null;
        }

        protected virtual void OnTriggerEnter2D(Collider2D other)
        {
            if(other.TryGetComponent<Rigidbody2D>(out var rigidbody2D))
                if(rigidbody2D.velocity.y < 0)
                    return;

            if(!other.CompareTag(Tags.Interactable.ToString()))
                return;

            if(!other.TryGetComponent<Interactable>(out var interactable))
                return;

            Attach(interactable);
        }

        protected void Attach(Interactable interactable)
        {
            if(currentInteractable != null && (interactable.Weight <= currentInteractable.Weight))
                return;

            if(interactable.Team < 0)
                return;

            if(interactable.IsLanded)
                return;

            if(currentInteractable != null)
            {
                currentInteractable.Detach();
                Detach();
            }

            Monster.Instance.Animator.SetTrigger("Insert");
            GameplayManager.Instance.SoundManager.PlaySound(Monster.Instance.GenericAttachSounds[Random.Range(0, Monster.Instance.GenericAttachSounds.Count)]);
            Instantiate(Monster.Instance.InsertParticlePrefab, transform.position, Quaternion.identity);
            
            currentInteractable = interactable;

            interactable.SetSlot(this);

            currentInteractable.transform.rotation = Quaternion.identity;
            
    
            currentInteractable.transform.SetParent(transform);
            currentInteractable.transform.localPosition = Vector3.zero;



            currentInteractable.DisablePhysics();
            currentInteractable.SetLanded(currentInteractable.Team);
            currentInteractable.transform.localRotation = Quaternion.identity;

            interactable.Attached(this);

            SlotManager.Instance.CheckWinCondition();
        }
    }
}
