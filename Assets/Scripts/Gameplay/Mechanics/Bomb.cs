using System.Collections.Generic;
using GGJ2024.Gameplay;
using UnityEngine;

namespace Gameplay
{
    public class Bomb : Interactable
    {
        [SerializeField] private GameObject firstSpark;
        [SerializeField] private GameObject landedSpark;
        [SerializeField] private AudioClipSO fuseAudioClip;
        [SerializeField] private AudioSource audioSource;

        private float detonationDuration = 3f;
        private float detonationTimer = -1f;
        private float attachedTimer = 1;

        public override void Attached(Slot slot)
        {
            detonationTimer = Time.realtimeSinceStartup + attachedTimer;

            firstSpark.SetActive(false);
            landedSpark.SetActive(true);
        }

        public override void Detach()
        {
            base.Detach();

            firstSpark.SetActive(true);
            landedSpark.SetActive(false);
        }   

        public override void Kick(KickType forceType, KickDirection kickDirection, int team, Transform source)
        {
            firstSpark.SetActive(true);
            landedSpark.SetActive(false);

            base.Kick(forceType, kickDirection, team, source);
            StartDetonation();

            if(fuseAudioClip != null)
            {
                audioSource.clip = fuseAudioClip.AudioClip;
                audioSource.Play();
                audioSource.pitch = fuseAudioClip.Pitch;
                audioSource.volume = fuseAudioClip.Volume;
            }

        }

        private void StartDetonation()
        {
            detonationTimer = Time.realtimeSinceStartup + detonationDuration;
        }

        protected override void FixedUpdate()
        {
            base.FixedUpdate();
            if (detonationTimer > 0 && Time.realtimeSinceStartup > detonationTimer)
            {
                detonationTimer = -1f;

                Explode();
            }
        }

        private void Explode()
        {
            if(slot != null)
            {
                var slots = GetSlots(slot);
                slots.RemoveAll(x => !x.HasInteractable);

                foreach(var nearbySlot in slots)
                {
                    var direction = (slot.transform.position - nearbySlot.transform.position).normalized;
                    var force = 500f;

                    if(nearbySlot.HasInteractable)
                    {
                        nearbySlot.CurrentInteractable.Rigidbody2D.AddForce(direction * force, ForceMode2D.Impulse);
                    }
                }
    
                slots.ForEach(x => x.Detach());
            }

            var explosion = Instantiate(GameplayManager.Instance.ExplosionParticlePrefab, transform.position, Quaternion.identity);
            if(slot != null)
            {
                explosion.transform.parent = slot.transform;
                explosion.transform.localPosition = Vector3.zero;
            }

            Detach();
            if(slot)
            {
                slot.Detach();
            }
 
            Destroy(gameObject);

            CameraScreenshake.Instance.Shake(0.75f);

            if(groundHits > 0)
            {
                var minimumDistance = 3f;

                foreach(var interactable in List)
                {
                    if(interactable == this)
                        continue;

                    if(interactable.GroundHits < 1)
                        continue;
                    var distance = Vector2.Distance(transform.position, interactable.transform.position);
                    if(distance < minimumDistance)
                    {
                        Vector2 dir = interactable.transform.position - transform.position;
                        dir.y += 3f;
                        // Normalize

                        dir.Normalize();

                        var direction = dir.normalized;
                        var force2 = Random.Range(400, 600);

                        var finalForce = direction * force2;

                        // depend on distance

                        var distanceRatio = 1 - (distance / minimumDistance);

                        finalForce.x *= Mathf.Max(0.5f, distanceRatio);
                        finalForce.y *= Mathf.Max(0.75f, distanceRatio);

                        interactable.KickBlow(finalForce);
                    }
                }
            } 
        }
        
        private List<Slot> GetSlots(Slot slot)
        {
            if (slot is Lower)
            {
                return SlotManager.Instance.GetNearbySlotsBottom(slot);
            }
            else
            {
                return SlotManager.Instance.GetNearbySlotsTop(slot);
            }
        }
    }
}