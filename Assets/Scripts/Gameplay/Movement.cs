using Gameplay;
using GGJ2024.Gameplay.State;
using UnityEngine;
using System.Linq;

namespace GGJ2024.Gameplay
{
    public class Movement : MonoBehaviour
    {
        [SerializeField] private Unit unit;
        [SerializeField] private new Rigidbody rigidbody;
        [SerializeField] private new Collider collider;
        [SerializeField] private float speed;
        private float distanceToGround;

        private void FixedUpdate() 
        {
            if(!IsGrounded())
            {
                // apply gravity force
                rigidbody.AddForce(Physics.gravity * rigidbody.mass * 3);
            }
        }

        protected virtual void OnValidate()
        {
            if(!rigidbody)
                TryGetComponent(out rigidbody);

            if(!collider)
                TryGetComponent(out collider);

            if(!unit)
                TryGetComponent(out unit);
        }

        protected virtual void Start() 
        {
            distanceToGround = collider.bounds.extents.y;
        }
    
        public void Move(Vector2 direction)
        {
            var moveDirection = new Vector3(direction.x, 0, direction.y);

            if(unit.StateMachine.CurrentState is Jumping)
            {
                rigidbody.velocity = new Vector3(speed * moveDirection.x, rigidbody.velocity.y, speed * moveDirection.z);
            }

            if(!IsGrounded())
                return;

            if(moveDirection.magnitude > 0)
            {
                if(unit.StateMachine.CurrentState is Idle && IsGrounded())
                {
                    unit.StateMachine.SetState<Running>();
                }
            }
            else
            {
                if(unit.StateMachine.CurrentState is Running)
                {
                    unit.StateMachine.SetState<Idle>();
                }
            }

            if(direction == Vector2.zero)
            {
                Stop();
                return;
            }

            rigidbody.velocity = speed * moveDirection;
        }

        public void Jump()
        {
            if(!IsGrounded())
                return;

            rigidbody.AddForce(Vector3.up * 16f, ForceMode.Impulse);
        }

        public void MoveTowardsFinish()
        {
            GameObject finishPoint;
            if(unit.Team == 0)
            {
                finishPoint = GameplayManager.Instance.Player1FinishPoint;
            }
            else
            {
                finishPoint = GameplayManager.Instance.Player2FinishPoint;
            }

            var direction = finishPoint.transform.position - transform.position;
            direction.y = 0;
            direction.Normalize();

            if(direction.x > 0)
            {
                unit.LookRight();
            }
            else if(direction.x < 0)
            {
                unit.LookLeft();
            }

            if((finishPoint.transform.position - transform.position).magnitude > 0.8f)
            {
                Move(direction);
            }
            else
            {
                Stop();
                var landedInteractables = FindObjectsOfType<Interactable>().ToList();
                landedInteractables = landedInteractables.Where(interactable => interactable.IsLanded).ToList();
                
                var currentPlayerInteractablesCount = landedInteractables.Where(interactable => interactable.Team == unit.Team).Count();
                var otherPlayerInteractablesCount = landedInteractables.Count - currentPlayerInteractablesCount;

                if(unit.StateMachine.CurrentState is not Win && currentPlayerInteractablesCount > otherPlayerInteractablesCount)
                {
                    unit.StateMachine.SetState<Win>();
                }

                if(unit.StateMachine.CurrentState is not Idle && currentPlayerInteractablesCount < otherPlayerInteractablesCount)
                {
                    unit.StateMachine.SetState<Idle>();
                }
            }
        }

        public void Stop()
        {
            rigidbody.velocity = Vector3.zero;
        }

        public bool IsGrounded()
        {
            var allLayersButNotPlayer = ~LayerMask.GetMask("Player");
            return Physics.Raycast(transform.position, Vector3.down, distanceToGround + 0.1f,allLayersButNotPlayer );
        }
    }
}