using GGJ2024.Helpers;
public class GameplaySettings : MonoSingleton<GameplaySettings>
{
    public float KickForce = 4000;
    public float Gravity = 5.81f;
    public float SpitGravityIncrease = 1.03f;
}
