using System.Collections.Generic;
using UnityEngine;

namespace Gameplay
{
    public class InteractableSpawner : MonoBehaviour
    {
        [SerializeField] private List<Interactable> interactables;

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Alpha1))
                Spawn(KickType.High, KickDirection.Left);

            if (Input.GetKeyDown(KeyCode.Alpha2))
                Spawn(KickType.Medium, KickDirection.Left);

            if (Input.GetKeyDown(KeyCode.Alpha3))
                Spawn(KickType.Low, KickDirection.Left);
        }

        private void Spawn(KickType kickType, KickDirection kickDirection)
        {
            var randomIndex = Random.Range(0, interactables.Count);
            var interactablePrefab = interactables[randomIndex];
            var instance = Instantiate(interactablePrefab, transform.position, Quaternion.identity);
            instance.name = interactablePrefab.name;

            // set z position to 0

            instance.transform.position = new Vector3(transform.position.x, transform.position.y, transform.position.z);

            if(instance.TryGetComponent<Interactable>(out var interactable))
            {
                interactable.Kick(kickType, kickDirection, 0, null);

            }
        }
    }
}
