using UnityEngine;

namespace Spinout.Helpers
{
    public class KeepRotation : MonoBehaviour
    {
        private Vector3 initialRotation;

        private void Awake() 
        {
            initialRotation = transform.rotation.eulerAngles;
        }

        private void Update()
        {
            transform.rotation = Quaternion.Euler(initialRotation);
        }
    }
}