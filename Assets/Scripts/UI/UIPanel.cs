using UnityEngine;

namespace GGJ2024.UI
{
    public abstract class UIPanel : MonoBehaviour
    {
        protected UIManager uiManager;

        private void OnEnable()
        {
            OnShow();
        }

        private void OnDisable()
        {
            OnHide();
        }

        protected virtual void OnShow()
        {
        }

        protected virtual void OnHide()
        {
        }

        public void Show(UIManager uiManager)
        {
            this.uiManager = uiManager;
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}