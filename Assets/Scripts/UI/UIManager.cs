using UnityEngine;

namespace GGJ2024.UI
{
    public class UIManager : MonoBehaviour
    {
        [SerializeField] private UIMainMenu mainMenu;

        public void ShowMainMenu()
        {
            mainMenu.Show(this);
        }

        public void HideMainMenu()
        {
            mainMenu.Hide();
        }
    }
}