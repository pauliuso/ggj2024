using System.Collections;
using System.Collections.Generic;
using GGJ2024.Helpers;
using UnityEngine;

public class CameraScreenshake : MonoSingleton<CameraScreenshake>
{
    private float shake = 0;    

    public void Shake(float amount)
    {
        shake = Mathf.Max(shake, amount);
    }

    public void Update()
    {
        if (shake > 0)
        {
            var shakePower = Mathf.Min(0.5f, shake);
            transform.localPosition = 0.5f * shakePower * Random.insideUnitSphere;
            shake -= Time.deltaTime;
        }
        else
        {
            shake = 0.0f;
            transform.localPosition = Vector3.zero;
        }
    }
}
