using GGJ2024.Gameplay;
using UnityEngine;

public class CameraPlayerFollower : MonoBehaviour
{
    [SerializeField] private float ratio = 0.2f;
    [SerializeField] Vector3 smoothPosition = Vector3.zero;

    private void Start()
    {
        smoothPosition = transform.position;
    }

    private void Update()
    {
        var position = transform.position;
        if(PlayerManager.Instance.GetPlayerCount() == 0)
        {
            smoothPosition = new Vector3(0, 0, position.z);
            return;
        }
        
        GetPlayersMiddlePosition();
    }

    private void GetPlayersMiddlePosition()
    {
        Unit player2 = null;

        if (!PlayerManager.Instance.TryGetPlayer(0, out Unit player1) && !PlayerManager.Instance.TryGetPlayer(1, out player2))
            return;
      
        var position = Vector3.zero;
        var count = 0;
        if(player1)
        {
            position.x += player1.transform.position.x * ratio;
            count++;
        }

        if(player2)
        {
            position.x += player2.transform.position.x * ratio;
            count++;
        }

        var middlePosition = position / count;
        smoothPosition = new Vector3(middlePosition.x, middlePosition.y, transform.position.z);

        transform.position = Vector3.Lerp(transform.position, smoothPosition, 0.1f);
    }
}
