using System.Collections;
using System.Collections.Generic;
using GGJ2024.Helpers;
using UnityEngine;

public class Fadeout : MonoSingleton<Fadeout>
{
    [SerializeField] SpriteRenderer spriteRenderer;

    bool isFadingOut = false;
    bool isFadingIn = false;

    private float alpha = 0f;

    public bool IsFadedOut => isFadingOut && alpha >= 1;

    public void FadeOut()
    {
        isFadingOut = true;
        isFadingIn = false;

        alpha = 0;
    }

    public void FadeIn()
    {
        isFadingOut = false;
        isFadingIn = true;

        alpha = 1.1f;
    }

    private void Update()
    {
        if (isFadingOut)
        {
            alpha += Time.deltaTime * 0.85f;

            if (alpha >= 1)
            {
                alpha = 1;
            }
        }
        else if (isFadingIn)
        {
            alpha -= Time.deltaTime * 0.75f;

            if (alpha <= 0)
            {
                alpha = 0;
                isFadingIn = false;
            }
        }

        spriteRenderer.color = new Color(1, 1, 1, alpha);
    }
}