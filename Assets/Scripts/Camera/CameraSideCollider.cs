using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraSideCollider : MonoBehaviour
{
    public static List<CameraSideCollider> List = new();

    public void Start()
    {
        List.Add(this);
    }

    public void OnDestroy()
    {
        List.Remove(this);
    }

    public enum Side
    {
        Left,
        Right
    }

    [SerializeField] private Side side;

    public Side SideValue => side;

    void Update()
    {
        if(side == Side.Right)
        {
            SetRightSide();
        }
        else
        {
            SetLeftSide();
        }
    }

    private void SetLeftSide()
    {
        var leftPixel = new Vector3(0, 0, transform.position.z - Camera.main.transform.position.z);

        var leftWorld = Camera.main.ScreenToWorldPoint(leftPixel);

        var position = transform.position;

        position.x = leftWorld.x;

        transform.position = position;
    }

    private void SetRightSide()
    {
        var rightPixel = new Vector3(Screen.width, 0, transform.position.z - Camera.main.transform.position.z);

        var rightWorld = Camera.main.ScreenToWorldPoint(rightPixel);

        var position = transform.position;

        position.x = rightWorld.x;

        transform.position = position;
    }
}
